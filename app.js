'use strict';

// simple express server
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/public'))

require('./app/routes.js')(app)

app.use(function(req, res) {
	res.sendFile('./public/index.html')
})

app.listen(5000);