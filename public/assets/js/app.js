var tabStateApp = angular.module('tabStateApp', ['ngRoute']);

tabStateApp.config(function($routeProvider) {
    $routeProvider

        .when('/', {
        	'name' : 'home',
            'templateUrl' : 'pages/home.html',
        })

        .when('/project', {
        	'name' : 'project',
            'templateUrl' : 'pages/project.html',
        })
});